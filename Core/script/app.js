(function($){

  $('#quranId .slider-bundle').not('.slick-initialized').slick({
      dots: true,
      infinite: true,
      speed: 200,
      //arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: $('#quranId .next-arrow'),
      prevArrow: $('#quranId .prev-arrow'),
    });

    $('#quran2 .slider-bundle').not('.slick-initialized').slick({
        dots: true,
        infinite: true,
        speed: 200,
        //arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: $('#quran2 .next-arrow'),
        prevArrow: $('#quran2 .prev-arrow'),
      });

  //video
    var youtubeID = $('.video-item').each(function () {
        $(this).attr('data-youtube');
    });

    $('.video-item').each(function(item, index){
        //var video = 'http://www.youtube.com/embed/' + $(this).data('youtube') + '?wmode=opaque?hd=1&rel=0&autohide=1&showinfo=0';
        var image = 'https://i.ytimg.com/vi/' + $(this).data('youtube') + '/hqdefault.jpg'
        //$(this).find('iframe').attr("src", video);
        $(this).find('img').attr("src", image);
        //$(this).append(video);
    });

    $('.yt-play').unbind('click').click(function(){
      var videoid = $(this).parents('.video-item').attr('data-youtube');
      var video = 'https://www.youtube.com/embed/' + (videoid) + '?wmode=opaque?hd=1&rel=0&autohide=1&showinfo=0&autoplay=1';
      var image = 'https://i.ytimg.com/vi/' + (videoid) + '/default.jpg';
      var srcvid = $(this).next('.yt-iframe').attr("src", video);
      $(this).hide();
    });

    $('.arrow.next-arrow').click(function(){
    var videoid = $('.yt-play').parents('.video-item').attr('data-youtube');
    var image = 'https://i.ytimg.com/vi/' + (videoid) + '/default.jpg';
    var searchVideo = $(this).parents().parents().find('.yt-iframe').attr("src"," ").find('img');
    $('.yt-play').css('display','block');
  });

  $('.arrow.prev-arrow').click(function(){
    var videoid = $('.yt-play').parents('.video-item').attr('data-youtube');
    var image = 'https://i.ytimg.com/vi/' + (videoid) + '/default.jpg';
    var searchVideo = $(this).parents().parents().find('.yt-iframe').attr("src"," ").find('img');
    $('.yt-play').css('display','block');
  });

  //menu
  $("#home").click(function() {
    $('html,body').animate({
        scrollTop: $("html").offset().top},
        'slow');

});

$("#quran").click(function() {
    $('html,body').animate({
        scrollTop: $(".section2").offset().top - 50},
        'slow');

});

$("#sketsa").click(function() {
    $('html,body').animate({
        scrollTop: $(".section4").offset().top - 50},
        'slow');

});


$("#sticker").click(function() {
    $('html,body').animate({
        scrollTop: $(".section5").offset().top - 50},
        'slow');

});

$("#ngabungirit").click(function() {
    $('html,body').animate({
        scrollTop: $(".section6").offset().top - 50},
        'slow');

});


})(jQuery);
